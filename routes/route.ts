import { Router } from "express";
import controller from '../controllers/controller';
const router = Router();

router.get('/', controller.homePage);

router.get('/companies', controller.getAllCompanies);

router.get('/contacts', controller.getAllContacts);

router.get('/deals', controller.getAllDeals);

module.exports = router;