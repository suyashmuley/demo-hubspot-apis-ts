import { Request, Response } from "express";
import axios, { AxiosResponse } from "axios";
import * as dotenv from 'dotenv';

dotenv.config();

const baseUri: string = 'https://api.hubapi.com/crm/v3/objects';
const API_KEY: string | undefined = process.env.API_KEY;

const homePage =async (req: Request, res: Response) => {
    res.send('Hello'); 
};


const getAllContacts =async (req: Request, res: Response) => {
    const getContactsUri: string = `${baseUri}/contacts?hapikey=${API_KEY}`;
    try {
        const resp: AxiosResponse = await axios.get(getContactsUri);
        const data: any = resp.data.results;
        // res.json(data);
        res.render('contacts', {data});
    } catch (error) {
        console.log(error)
    }
};

const getAllCompanies =async (req: Request, res: Response) => {
    const getCompaniesUri: string = `${baseUri}/companies?hapikey=${API_KEY}`;
    try {
        const resp: AxiosResponse = await axios.get(getCompaniesUri);
        const data: any = resp.data.results;
        // res.json(data);
        res.render('companies', {data});
    } catch (error) {
        console.log(error)
    }
};

const getAllDeals =async (req: Request, res: Response) => {
    const getDealsUri: string = `${baseUri}/deals?hapikey=${API_KEY}`;
    try {
        const resp: AxiosResponse = await axios.get(getDealsUri);
        const data: any = resp.data.results;
        // res.json(data);
        res.render('deals', {data});
    } catch (error) {
        console.log(error)
    }
};

export default {homePage, getAllCompanies, getAllContacts, getAllDeals};